<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferSubscription extends Model
{
    protected $fillable =[
        'offer_id',"student_id"
    ];

    /** RELACIONES **/ //Aquí las relaciones
    public function offer()
    {
        return $this->belongsTo('App\Offer');
    }
    public function student()
    {
        return $this->belongsTo('App\Student');
    }



    /** GETTERS **/ //Aquí los getters






    /** SETTERS **/ //Aquí los setters






    /** SCOPES **/ //Aquí los scopes



}
