<?php

namespace App\Http\Controllers;


/** REQUEST */
use Illuminate\Http\Request;

/** MODELOS */
use App\Cicle;
use App\Family;

/** MAILS */

/** OTROS */
use Illuminate\Support\Facades\Session;

class CicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $formativeCicles = Cicle::Name($request)->paginate(10);

        return view("cicles.index",compact("formativeCicles","request"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $familys = get_model_selectable_by_name(Family::all());
        return view('cicles.create',compact("familys"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cicle_opts = [
            "nombre" => $request->get("name"),
            "tipo" => $request->get("tipo"),
            "plan" => $request->get("plan"),
            "family_id" => $request->get("family_id")
        ];
        $cicle = new Cicle($cicle_opts);
        $cicle->save();
        Session::flash('message', 'Ciclo Formativo agregado correctamente');
        return redirect()->route("cicle.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cicle  $cicle
     * @return \Illuminate\Http\Response
     */
    public function show(Cicle $cicle)
    {
        //
        return view("cicles.show",compact("cicle"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cicle  $cicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Cicle $cicle)
    {
        $familys = get_model_selectable_by_name(Family::all());
        return view("cicles.edit",compact("cicle","familys"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cicle  $cicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cicle $cicle)
    {
        $cicle->fill(["name" => $request->get("name"),"tipo"=>$request->get("tipo"),"plan"=>$request->get("plan")]);
        $cicle->save();
        Session::flash('message', 'Ciclo Profesional editado correctamente');
        return redirect()->route("cicle.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cicle  $cicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cicle $cicle)
    {
        $name = $cicle->name;
        $cicle->delete();
        Session::flash('message', 'El ciclo formativo '.$name.' se ha eliminiado correctamente');
        return redirect()->route("cicle.index");
    }
}
