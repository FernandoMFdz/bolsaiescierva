<?php

namespace App\Http\Controllers;

/** REQUEST */
use Illuminate\Http\Request;
use App\Http\Requests\EnterpriseStoreRequest;
use App\Http\Requests\EnterpriseUpdateRequest;

/** MODELOS */
use App\Offer;
use App\Enterprise;
use App\User;

/** MAILS */
use App\Mail\newRegisteredEnterprise;
use Illuminate\Support\Facades\Mail;

/** OTROS */
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Session;

class EnterpriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $empresas= Enterprise::Search($request)->Active()->paginate(10);

        return view('enterprise.index', compact('empresas','request'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('enterprise.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EnterpriseStoreRequest $request)
    {
        $user= new User($request->only(["name","email","password"]));
        $user->phone = $request->get("phone");
        $user->is_active = false;
        $user->save();

        $user->enterprise()->create($request->only(["descripcion","sociedad","cif","fax","fecha_fundacion","web","pais","ciudad","min_empleados","max_empleados"]));

        if($user->enterprise->save()){
            Session::flash('message', 'Empresa agregada correctamente');
            /*
             * Email, sólo nesitamos darle el email  + datos del usuario.
             */
            Mail::to($user->email)->send(new newRegisteredEnterprise($user));
            return redirect()->route("enterprise.index");
        }else {
            return "ERROR";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function show(Enterprise $enterprise)
    {
        $offers = $enterprise->offers;
        return view('enterprise.show',compact('enterprise',"offers"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function edit(Enterprise $enterprise)
    {
        Session::flash('user_id', $enterprise->user->id);
        $user = $enterprise->user->toArray();
        unset($user["id"]);
        $user = array_merge($enterprise->toArray(),$user);
        $empresa = new Collection($user);

        return view('enterprise.edit',compact('empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function update(EnterpriseUpdateRequest $request, Enterprise $enterprise)
    {
        $enterprise->user->fill($request->only(["name","email"]));
        $enterprise->user->phone = $request->get("phone");
        $enterprise->user->save();
        $enterprise->fill($request->only(["descripcion","sociedad","cif","fax","fecha_fundacion","web","pais","ciudad","min_empleados","max_empleados"]));

        if($enterprise->save()){
            Session::flash('message', 'Empresa editada correctamente');
            return redirect()->route("enterprise.index");

        }else {
            return "Error en la Edicion";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Enterprise $enterprise)
    {
        $enterprise->user()->delete();
        $enterprise->delete();

        return redirect()->route("enterprise.index");
    }

    public function inactive()
    {
        $enterprises = Enterprise::NoActive()->get();
        return view("enterprise.inactive", compact("enterprises"));
    }

}
