<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Family extends Model
{
    protected $fillable =[
       'name',"created_at"
    ];

    /** RELACIONES **/ //Aquí las relaciones
    public function cicles()
    {
        return $this->hasMany('App\Cicle');
    }

    public function offer()
    {
        return $this->hasMany('App\Offer');
    }





    /** GETTERS **/ //Aquí los getters






    /** SETTERS **/ //Aquí los setters






    /** SCOPES **/ //Aquí los scopes
    public function scopeName($query,Request $request)
    {
        $name = $request->get("name");
        if (trim($name) != '') {
            $query->where('name','LIKE',"%$name%");
        }
    }
}
