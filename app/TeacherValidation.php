<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherValidation extends Model
{
    protected $fillable =[
        'id',"teacher_id","user_id", "action", "created_at","updated_at"
    ];

    /** RELACIONES **/ //Aquí las relaciones
    public function teacher()
    {
        return $this->belongsTo('App\Teacher');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }


    public function getRolAttribute()
    {
        switch ($this->user->rol){
            case "is_student":
                return "Alumno";
                break;
            case "is_teacher":
                return "Profesor";
                break;
            case "is_admin":
                return "Administrador";
                break;
            case "is_enterprise":
                return "Empresa";
                break;

        }
    }





    /** GETTERS **/ //Aquí los getters






    /** SETTERS **/ //Aquí los setters






    /** SCOPES **/ //Aquí los scopes

    public function scopeAction($query,$action)
    {
        if(trim($action) != '') {
            $query->where('action', '=', $action);
        }

    }

}
