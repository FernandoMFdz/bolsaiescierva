<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OfferSelection extends Model
{
    protected $fillable =[
        'offer_id',"student_id","teacher_id","answer"
    ];

    /** RELACIONES **/ //Aquí las relaciones
    public function teacher()
    {
        return $this->belongsTo('App\Teacher');
    }
    public function offer()
    {
        return $this->belongsTo('App\Offer');
    }
    public function student()
    {
        return $this->belongsTo('App\Student');
    }

    public function answer()
    {
        return $this->hasOne('App\SelectionAnswer');
    }





    /** GETTERS **/ //Aquí los getters
    





    /** SETTERS **/ //Aquí los setters






    /** SCOPES **/ //Aquí los scopes
    public function scopeAuthPending($query)
    {

        return $query->where('student_id',auth()->user()->student->id)->where("answer","2");
    }
    
}
