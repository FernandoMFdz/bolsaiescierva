<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{

    //Inluir Softdeletes

    public $timestamps = false;
    protected $fillable = [
        'apellidos', 'nre', 'vehiculo', 'domicilio', 'status', 'edad',
    ];

    /** RELACIONES **/ //Aquí las relaciones

    //Relacion con user
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    //Relacion con OfferSelections
    public function selections()
    {
        return $this->hasMany('App\OfferSelection');
    }

    public function selectionsPositive()
    {
        return $this->hasMany('App\OfferSelection')->where("student_id",$this->id)->where("answer","1");
    }
    public function selectionsPending()
    {
        return $this->hasMany('App\OfferSelection')->where("student_id",$this->id)->where("answer","2");
    }

    public function selectionsNegative()
    {
        return $this->hasMany('App\OfferSelection')->where("student_id",$this->id)->where("answer","0");
    }

    //Relacion con OfferSubscriptions
    public function subscriptions()
    {
        return $this->hasMany('App\OfferSubscription');
    }

    //Relacion con ciclos
    public function cicles()
    {
        return $this->belongsToMany('App\Cicle', 'student_courses', 'student_id', 'cicle_id')->withPivot('cicle_id', 'promocion');
    }


    /** SETTERS **/ //Aquí los setters
    public function setVehiculoAttribute($value)
    {
        if (trim($value) != '') {
            $this->attributes['vehiculo'] = $value;
        } else {
            $this->attributes['vehiculo'] = 0;
        }
    }







    /** SCOPES **/ //Aquí los scopes

    //Scope para el apellido del alumno
    public function scopeApellido($query, $apellido)
    {

    }

    //Scope para el nre del alumno
    public function scopeNre($query, $nre)
    {

    }

    //Scope para el vehiculo del alumno
    public function scopeVehiculo($query, $vehiculo)
    {

    }

    //Scope para el domicilio del alumno
    public function scopeDomicilio($query, $domicilio)
    {

    }

    //Scope para el status del alumno
    public function scopeStatus($query, $status)
    {

    }

    //Scope para la edad del alumno
    public function scopeEdad($query, $edad)
    {

    }

    public function scopeDatos($query, $datos)
    {

        //Cambiar
        return $query->orWhere('apellidos', 'like', "%" . $datos . "%")
            ->orWhere('domicilio', 'like', "%" . $datos . "%");

    }

    public function scopeNoActive($query)
    {
        $query->with("user")->whereHas('user', function ($q) {
            //Reutilizo el scopeActive del modelo User.
            $q->Active(0);
        });
    }

    public function scopeActive($query)
    {
        $query->with("user")->whereHas('user', function ($q) {
            //Reutilizo el scopeActive del modelo User.
            $q->Active(1);
        });
    }


}
