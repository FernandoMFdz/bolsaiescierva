$(document).ready(function() {
    $('#table-data').DataTable();

    $('.logout-button').click(function () {

        $("#logout-form").submit();

    });
    
    $('.fullScreen-button').click(function () {
        fullScreen();
    });

    $(".slimScroll").slimScroll({
        height: "350px"
    });

    $(".slimScrollOffer").slimScroll({
        height: "50px"
    });

    $(".check-padre").click(function () {
        var id = $(this).attr("id");
        if(this.checked) {
            // Iterate each checkbox
            $(':checkbox[id="'+id+'"]').each(function() {
                this.checked = true;
            });
        }else{
            $(':checkbox[id="'+id+'"]').each(function() {
                this.checked = false;
            });
        }
    });

   // $('#multi-select').multiSelect();

} );


function fullScreen(elem) {
    var elem = elem || document.documentElement;
    if (!document.fullscreenElement && !document.mozFullScreenElement &&
        !document.webkitFullscreenElement && !document.msFullscreenElement) {
        if (elem.requestFullscreen) {
            elem.requestFullscreen();
        } else if (elem.msRequestFullscreen) {
            elem.msRequestFullscreen();
        } else if (elem.mozRequestFullScreen) {
            elem.mozRequestFullScreen();
        } else if (elem.webkitRequestFullscreen) {
            elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
        }
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
}