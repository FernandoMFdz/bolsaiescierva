<table class="table table-hover " id="table-data">
    <thead>
        <tr>
            <th>#</th>
            <th>Nombre</th>
            <th>NRP</th>
            <th>Email</th>
            <th>Telefono</th>
            <th>Admin</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    @foreach($teachers as $teacher)
        <tr>
            <td>
                {{$teacher->id}}
            </td>
            <td>
                <a href="{{route("user.profile",$teacher->user->id)}}">{{$teacher->apellidos}}, {{$teacher->user->name}} </a>
            </td>
            <td>
                {{$teacher->nrp_expediente}}
            </td>
            <td>
                {{$teacher->user->email}}
            </td>
            <td>
                {{$teacher->user->phone}}
            </td>
            <td>
                {{$teacher->is_admin}}
            </td>

            <td>
                <a href="{{route("teacher.edit",$teacher->id)}}"><button href="#" class="btn btn-link">Editar</button></a>
            </td>
            <td>
                {!! Form::open(['method' => 'DELETE', 'route' => ['teacher.destroy', $teacher->id]]) !!}
                    <button href="#" class="btn btn-link" type="submit">Eliminar</button>
                {!! Form::close() !!}
            </td>
        </tr>

    @endforeach
</table>