@foreach($profesionalfamilys as $profesionalfamily)
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><a href="{{route("family.show",$profesionalfamily->id)}}">{{$profesionalfamily->name}}</a></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false"><i
                                    class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#"><a class="btn btn-link"
                                               href="{{route("family.edit",$profesionalfamily->id)}}">Editar</a></a>
                            </li>
                            <li>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['family.destroy', $profesionalfamily->id]]) !!}
                                <button href="#" class="btn btn-link close-link" type="submit">Eliminar</button>
                                {!! Form::close() !!}
                            </li>
                        </ul>
                    </li>

                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="display: none;">
                @foreach($profesionalfamily->cicles as $ciclo)
                    <div class="panel panel-default">
                        <h5 class="text-center"><a href="{{route("cicle.show",$ciclo->id)}}">{{$ciclo->name}}</a></h5>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endforeach