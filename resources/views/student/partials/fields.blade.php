@if(\Route::currentRouteName() != "register")
    {{ Form::label('email', 'Correo electrónico') }}
@endif
{{ Form::text('email', null, ['class' => 'form-control', "placeholder" => "Email"]) }}


@if(\Route::currentRouteName() != "register")
    {{ Form::label('name', 'Nombre') }}
@endif
{{ Form::text('name', null, ['class' => 'form-control', "placeholder" => "Nombre"]) }}

@if(\Route::currentRouteName() != "register")
    {{ Form::label('apellidos', 'Apellidos') }}
@endif
{{ Form::text('apellidos', null, ['class' => 'form-control', "placeholder" => "Apellidos"]) }}

@if(\Route::currentRouteName() != "register")
    {{ Form::label('password', 'Contraseña') }}
@endif
{{ Form::password('password', ['class' => 'form-control', "placeholder" => "Contraseña"]) }}

@if(\Route::currentRouteName() != "register")
    {{ Form::label('password_confirmation', 'Repetir Contraseña') }}
@endif
{{ Form::password('password_confirmation', ['class' => 'form-control', "placeholder" => "Repetir Contraseña"]) }}

@if(\Route::currentRouteName() != "register")
    {{ Form::label('phone', 'Telefono') }}
@endif
{{ Form::text('phone', null, ['class' => 'form-control', "placeholder" => "Teléfono"]) }}

@if(\Route::currentRouteName() != "register")
    {{ Form::label('nre', 'Número regional del estudiante (NRE)') }}
@endif
{{ Form::text('nre', null, ['class' => 'form-control', "placeholder" => "Número regional del estudiante (NRE)"]) }}

<div class="form-group">
    {{ Form::label('cicle_id',"Ciclo formativo") }}
    {{ Form::select('cicle_id', $cicles, null, ['class' => 'form-control',"placeholder" => "Ciclos Formativos"]) }}
</div>
<div class="form-group">
    {{ Form::label('start_date',"Fecha comienzo") }}
    {{ Form::date('start_date', \Carbon\Carbon::now()) }}
</div>
<div class="form-group">
    {{ Form::label('end_date',"Fecha fin") }}
    {{ Form::date('end_date', \Carbon\Carbon::now()) }}
</div>
<div class="form-group">
    {{ Form::label('promocion',"Promocion") }}
    {{ Form::text('promocion',null,['class'=>'form-control','placeholder' => "Ej: 2017"]) }}
</div>
@if(\Route::currentRouteName() != "register")
    {{ Form::label('domicilio', 'Domicilio') }}
@endif
{{ Form::text('domicilio', null, ['class' => 'form-control', "placeholder" => "Domicilio"]) }}

@if(\Route::currentRouteName() != "register")
    {{ Form::label('status', 'Estado del alumno') }}
@endif
{{ Form::select('status',['ESTUDIANDO' => "Estudiando", "FCT" => "En FCT", "CONTRATADO" => "Contratado", "PARO" => "En paro"], "ESTUDIANDO", ['class' => 'form-control']) }}
<br>
@if(\Route::currentRouteName() != "register")
    {{ Form::label('edad', 'Edad') }}
@endif
{{ Form::text('edad', null, ['class' => 'form-control', "placeholder" => "Edad"]) }}

<div class="checkbox">
    <label> {{ Form::checkbox('vehiculo')}} Vehiculo Propio</label>
</div>
